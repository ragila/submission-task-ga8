class BangunDatar{
    constructor() {
        if (this.constructor === BangunDatar) {
            throw new Error("Cannot instantiate from Abstract Class")
        }
        this.nama = this.constructor.name;
    }

    getName(){
        console.log(`Nama bangun datar ini ${this.nama}`)
    }    
}

class Persegi extends BangunDatar{
    constructor(sisi){
        super();
        this.sisi = sisi;
    }

    getName(nama){
        console.log(`ini getname persegi ${nama}`)
    }    

    keliling() {
        return 4 * this.sisi;
    }

    luas() {
        return this.sisi * this.sisi;
    }    
}

class PersegiPanjang extends BangunDatar{
    constructor(panjang, lebar){
        super();
        this.panjang = panjang;
        this.lebar = lebar;
    }

    keliling() {
        return 2 * ( this.panjang + this.lebar );
    }

    luas() {
        return this.panjang * this.lebar;
    }    
}

class JajarGenjang extends BangunDatar{
    constructor(sisi_bawah, sisi_atas, tinggi){
        super();
        this.sisi_bawah = sisi_bawah;
        this.sisi_atas = sisi_atas;
        this.tinggi = tinggi;
    }

    keliling() {
        return 2 * (this.sisi_bawah + this.sisi_atas);
    }

    luas() {
        return this.sisi_bawah * this.tinggi;
    }    
}

// objek
const persegiku = new Persegi(2);
const persegiPanjangku = new PersegiPanjang(2, 4);
const jajar = new JajarGenjang(3, 3, 4);
const jajar2 = new JajarGenjang(5, 5, 10);

const bangundatar = new BangunDatar()

persegiku.getName('ini nama');
console.log("Keliling : "+persegiku.keliling());
console.log("Luas : "+persegiku.luas());

persegiPanjangku.getName();
console.log("Keliling : "+persegiPanjangku.keliling());
console.log("Luas : "+persegiPanjangku.luas());

jajar.getName();
console.log("Keliling : "+jajar.keliling());
console.log("Luas : "+jajar.luas());

jajar2.getName();
console.log("Keliling : "+jajar2.keliling());
console.log("Luas : "+jajar2.luas());