import {takeLatest, put} from 'redux-saga/effects';
import {apiFetchInterests} from '../../common/api/interests';
import {ToastAndroid} from 'react-native';
import {getAccountId, getHeaders} from '../../common/function/auth';

function* getInterests(action) {
  try {
    console.info('1 iniii coba')
    const headers = yield getHeaders();
    const accountId = yield getAccountId();
    console.info('2')
    // FETCH PROFILE DATA
    const resProfile = yield apiFetchInterests(accountId, headers);
    yield put({type: 'GET_INTERESTS_SUCCESS', payload: resProfile.data});
  } catch (e) {
    // show alert
    ToastAndroid.showWithGravity(
      'Gagal mengambil data interests',
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM,
    );

    yield put({type: "GET_INTERESTS_FAILED"});
  }
}

function* interestsSaga() {
  yield takeLatest("GET_INTERESTS", getInterests);
}

export default interestsSaga;
