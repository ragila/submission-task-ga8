import {takeLatest, put} from 'redux-saga/effects';
import {apiLogin} from '../../common/api/auth';
import {ToastAndroid} from 'react-native';
import {removeToken, saveAccountId, saveToken} from '../../common/function/auth';
import {LOGIN, LOGIN_FAILED, LOGIN_SUCCESS, LOGOUT} from '../action/auth_types';
import {getProfileDetail} from '../action/profile';
import {getInterests} from '../action/interests.action'
import AsyncStorage from '@react-native-async-storage/async-storage';

function* login(action) {
  try {
    // LOGIN
    console.info('1');
    const resLogin = yield apiLogin(action.payload);
    console.info('2');

    if (resLogin && resLogin.data) {
      // console.info('a');
      // save token to local storage
      yield saveToken(resLogin.data.access_token);
      yield saveAccountId(resLogin.data.id);
      // console.info('b');

      yield put(getProfileDetail());
      // console.info('c');

      yield put({type: LOGIN_SUCCESS});
      // console.info('d');

      ToastAndroid.showWithGravity(
        `Hi, Engineer.  jungle.`,
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
    } else {
      // show alert
      ToastAndroid.showWithGravity(
        'Login gagal',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
      yield put({type: LOGIN_FAILED});
    }
  } catch (e) {
    // console.info('e', e);
    // show alert
    ToastAndroid.showWithGravity(
      'Gagal login',
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM,
    );

    yield put({type: LOGIN_FAILED});
  }
}

function* logout(action) {
  try {
    yield removeToken;
  } catch (e) {
    ToastAndroid.showWithGravity(
      'Gagal logout',
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM,
    );
  }
}

function* authSaga() {
  yield takeLatest(LOGIN, login);
  yield takeLatest(LOGOUT, logout);
}

export default authSaga;
