export const loginAction = (payload) => ({
  type: 'LOGIN',
  payload
});
  
export const logoutAction = () => {
    return {type: 'LOGOUT'};
};
  