const initialState = []

const todos = (state = initialState, action) => {
    switch(action.type) {
        case 'ADD TODO':
            return[
                ...state,
                {
                    id          :action.id,
                    text        :action.text,
                    completed   : false 
                }
            ];
            default:
                return state;
    }
} 

export default todos;