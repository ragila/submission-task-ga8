import {LOGIN, LOGIN_SUCCESS, LOGIN_FAILED, LOGOUT} from '../action/auth_types'
const initialState = {
    username: null,
    isLoading: false,
    isLoggedIn: false,
    data: {},
  };
  
  const auth = (state = initialState, action) => {
    switch (action.type) {
      case LOGIN:
        return {
          ...state,
          isLoading   : true
        };
      case LOGIN_SUCCESS:
        return {
          ...state,
          isLoggedIn  : true,
          isLoading   : false
        };
        case LOGIN_FAILED:
        return {
          ...state,
          isLoading   : false
        };
        case LOGOUT:
        return {
          ...state,
          isLoggedIn  : false,
          isLoading   : false
        };
      default:
        return state;
    }
  };
  
  export default auth;
  