import axios from 'axios';

export function apiFetchInterests(user_id, headers) {
  return axios({
    method: 'GET',
    url: 'http:/localhost:3000/interest/' + user_id,
    headers,
  });
}
