import React from 'react'
import {View, Text, Button, TextInput} from 'react-native'
import {addTodo} from '../redux/action/todo'
import {connect} from 'react-redux'

class TodoScreen extends React.Component {
    constructor(props){
        super(props);
        this.state= {
            newTodo: ''
        }
    }


    render(){
        return(
            <View style={{flex: 1, justifyContent: 'center'}}>
            <Text>TODO</Text>
            {this.props.todos.map((item, index) => (
                <View key={index}>
                    <Text>
                        {item.id + 1}. {item.text} --{''}
                        {item.completed ? 'DONE' : 'IN PROGRESS'}
                    </Text>
                </View>
            ))}

            <View>
                <TextInput 
                    placeholder={'Type new task here'}
                    onChangeText={(text) => this.setState({newTodo: text})}
                    style={{backgroundColor: '#ffffff', padding: 10}}
                />
            </View>
            <Button 
                title="ADD TO DO"
                onPress={() => this.props.addTodo(this.state.newTodo)}>

            </Button>
        </View>
        )
    }
}

const mapStateToProps = (state) => ({
    todos: state.todos,
})

const mapDispatchToProps = (dispatch) => ({
    addTodo: (text) => dispatch(addTodo(text)),
})

export default connect(mapStateToProps, mapDispatchToProps)
(TodoScreen)
