import React, {useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator
} from 'react-native';
import {connect} from 'react-redux';
import {loginAction} from '../redux/action/auth';

const {width, height} = Dimensions.get('window');
function LoginScreen(props) {
  const [username, setUsername] = useState(null);
  const [password, setPassword] = useState(null);
  const [message, setMessage]   = useState(null)

  const login = () => {
    if(!username){
      setMessage('Username wajib diisi')
    } else if (!password){
      setMessage('Password wajib diisi')
    } else {
      setMessage(null)
      props.processLogin({username, password})
      setUsername(null)
      setPassword(null)
    }
  }

  return (
    <ScrollView>
      <View style={[styles.containerCenterMiddle, {marginTop: 100}]}>
        <Text style={[styles.title, styles.textCenter]}>LOGIN PAGE</Text>
        <Text style={[styles.textCenter, {color: 'red'}]}>{message}</Text>

        <View style={{marginTop: 20}}>
          <TextInput
            onChangeText={(text) => setUsername(text)}
            value={username}
            placeholder="Username"
            style={styles.formTextInput}
          />
          <TextInput
            onChangeText={(text) => setPassword(text)}
            value={password}
            secureTextEntry={true}
            placeholder="Password"
            style={styles.formTextInput}
          />
        </View>

        {props.isLoading ? (
          <ActivityIndicator size="large" color="#333333" />
        ) : (
          <TouchableOpacity
          style={{
            backgroundColor: '#999999',
            padding: 15,
            borderRadius: 50,
            alignItems: 'center',
          }}
          onPress={() => login()}>
          <Text style={{color: '#ffffff'}}>LOGIN</Text> 
        </TouchableOpacity>
        )}
      </View>
    </ScrollView>
  );
}

const mapStateToProps = (state) => ({
  isLoading : state.auth.isLoading
});

const mapDispatchToProps = (dispatch) => ({
  processLogin: (data) => dispatch(loginAction(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);

const styles = StyleSheet.create({
  title: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  textCenter: {
    textAlign: 'center',
  },
  containerCenterMiddle: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    padding: 40,
  },
  formTextInput: {
    backgroundColor: '#ffffff',
    marginBottom: 20,
    borderRadius: 50,
    paddingHorizontal: 20,
    height: 45
  },
});
