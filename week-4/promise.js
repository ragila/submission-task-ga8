// const UserProfile = function(id) {
//     return (new Promise(function(resolve, reject) {
//         if (!id) {
//             reject(new Error('Invalid user id'))
//         }
//         let response = { 
//             succes  : true,
//             id      : id,
//             message : 'user found'
//         }
//         resolve(response)
//     }))
// }

// async function main() {
//     try {
//         const result    = await UserProfile(1)
//         console.log(result)
//     } catch (e) {
//         console.log('user not found')
//     }
// }

// main()

const request = require('request');
const requestPromise = require('request-promise')

const options = {
            url: 'https://api.github.com/repos/request/request', 
            headers: {
                'User-Agent': 'request' 
                }
            };
function callback(error, response, body) 
    {if (!error && response.statusCode == 200) {
            const info = JSON.parse(body); 
            console.log(info.name) 
            console.log(info.stargazers_count + " Stars"); 
            console.log(info.forks_count + " Forks");
}}

request(options, callback );


requestPromise(options).then((error, response, body) => {
    console.info(JSON.parse(response))
    if (!error && response.statusCode == 200) {
        const info = JSON.parse(body); 
        console.log(info.name) 
        console.log(info.stargazers_count + " Stars"); 
        console.log(info.forks_count + " Forks");
    }
}).catch(err => {
    console.error('ERROR')
}) 



 